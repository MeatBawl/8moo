# 8moo/commands/attack.py
from evennia import create_script
from command import Command

class attack(Command):
	"""
	Usage:
	  attack <target>

	Often used to kill people
	"""

	key = "attack"
	aliases = ["kill"]
	help_category = "General"

	def func(self):
		"This performs the actual command"
		errmsg = "You can't attack that!"
		if not self.args:
			self.caller.msg(errmsg)
			return
		try:
			target = self.args
		except ValueError:
			self.caller.msg(errmsg)
			return

		# at this point the argument is tested as valid. Let's set it.
		self.caller.msg("You attack%s (hypothetically)." % target)
		target = self.caller.search(self.args)
		if not target:
			self.caller.msg(errmsg)
			return

		if target.ndb.combat_handler:
			# target is already in combat - join it
			target.ndb.combat_handler.add_character(self.caller)
			target.ndb.combat_handler.msg_all("%s joins combat!" % self.caller)
		else:
			# create a new combat handler
			chandler = create_script("combat_handler.CombatHandler")
			chandler.add_character(self.caller)
			chandler.add_character(target)
			self.caller.msg("You attack %s! You are in combat." % target)
			target.msg("%s attacks you! You are in combat." % self.caller)
