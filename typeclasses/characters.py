"""
Characters

Characters are (by default) Objects setup to be puppeted by Players.
They are what you "see" in game. The Character class in this module
is setup to be the "default" character type created by the default
creation commands.

"""
from evennia import DefaultCharacter
from world.rules import roll_challenge

class Character(DefaultCharacter):
	"""
	The Character defaults to reimplementing some of base Object's hook methods with the
	following functionality:

	at_basetype_setup - always assigns the DefaultCmdSet to this object type
					(important!)sets locks so character cannot be picked up
					and its commands only be called by itself, not anyone else.
					(to change things, use at_object_creation() instead).
	at_after_move(source_location) - Launches the "look" command after every move.
	at_post_unpuppet(player) -	when Player disconnects from the Character, we
					store the current location in the pre_logout_location Attribute and
					move it to a None-location so the "unpuppeted" character
					object does not need to stay on grid. Echoes "Player has disconnected"
					to the room.
	at_pre_puppet - Just before Player re-connects, retrieves the character's
					pre_logout_location Attribute and move it back on the grid.
	at_post_puppet - Echoes "PlayerName has entered the game" to the room.

	"""
	def at_object_creation(self):
		"This is called when object is first created, only."
		self.db.health = 100
		self.db.health_max = 100
		self.db.hunger = 0
		self.db.thirst = 0
		self.db.strength = 10
		self.db.speed = 10
		self.db.blind = False
		self.db.deaf = False
		self.db.detects_atmos = False

	def at_before_move(self, destination):
		if self.ndb.combat_handler:
			self.msg("in combat")
			escapeSuccess = True
			escapeChallengers = []
			for character in self.ndb.combat_handler.db.characters.values():
				if character.id != self.id:
					escapeChallengers.append(character)
					self.msg(character.id)
			for challenger in escapeChallengers:
				if roll_challenge(self, challenger, "speed") == True:
					self.msg("success")
				else:
					escapeSuccess = False
					self.msg("fail")
			return False
		else:
			print "not in combat"
			return True
