# 8moo/typeclasses/combat_handler.py

import random
from evennia import DefaultScript

class CombatHandler(DefaultScript):
	def at_script_creation(self):
		"Called when script is first created"

		self.key = "combat_handler_%i" % random.randint(1, 1000)
		self.desc = "handles combat"
		self.interval = 60 * 2	# two minute timeout
		self.start_delay = True
		self.persistent = True	 

		# store all combatants
		self.db.characters = {}
		# store all actions for each turn
		self.db.turn_actions = {}
		# number of actions entered per combatant
		self.db.action_count = {}

	def _init_character(self, character):
		"""
		This initializes handler back-reference 
		and combat cmdset on a character
		"""
		character.ndb.combat_handler = self

	def _cleanup_character(self, character):
		"""
		Remove character from handler and clean 
		it of the back-reference and cmdset
		"""
		dbref = character.id 
		del self.db.characters[dbref]
		del self.db.turn_actions[dbref]
		del self.db.action_count[dbref]
		del character.ndb.combat_handler

	def at_start(self):
		"""
		This is called on first start but also when the script is restarted
		after a server reboot. We need to re-assign this combat handler to 
		all characters as well as re-assign the cmdset.
		"""
		for character in self.db.characters.values():
			self._init_character(character)

	def at_stop(self):
		"Called just before the script is stopped/destroyed."
		for character in list(self.db.characters.values()):
			# note: the list() call above disconnects list from database
			self._cleanup_character(character)

	def at_repeat(self, *args):
		"""
		This is called every self.interval seconds or when force_repeat
		is called (because everyone has entered their commands).	   

		We let this method take optional arguments (using *args) so we can separate
		between the timeout (no argument) and the controlled turn-end
		where we send an argument.
		"""
		pass
	# Combat-handler methods

	def add_character(self, character):
		"Add combatant to handler"
		dbref = character.id
		self.db.characters[dbref] = character		 
		self.db.action_count[dbref] = 0
		self.db.turn_actions[dbref] = [("defend", character, None), ("defend", character, None)]
		# set up back-reference
		self._init_character(character)

	def remove_character(self, character):
		"Remove combatant from handler"
		if character.id in self.db.characters:
			self._cleanup_character(character)
		if not self.db.characters:
			# if we have no more characters in battle, kill this handler
			self.stop()

	def msg_all(self, message):
		"Send message to all combatants"
		for character in self.db.characters.values():
			character.msg(message)
