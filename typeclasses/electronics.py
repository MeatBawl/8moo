# electronics
from typeclasses.objects import Object

class electronics(Object):
	def at_object_creation(self):
		super(electronics, self).at_object_creation()
		self.key = "Generic Electronics"
		self.db.desc = "this item isn't supposed to exist"

class atmosDetector(electronics):
	def at_object_creation(self):
		super(atmosDetector, self).at_object_creation()
		self.key = "Atmospheric Detector"
		self.db.desc = "This device detects changes in pressure."
	def at_get(self, getter):
		getter.db.detects_atmos = True
	def at_drop(self, dropper):
		dropper.db.detects_atmos = False
