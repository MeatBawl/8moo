# npc
from characters import Character

class npc(Character):
	def at_object_creation(self):
		super(npc, self).at_object_creation()
		self.key = "Generic NPC"
		self.db.desc = "a generic NPC, he's not supposed to be spawned like this."

	def at_char_entered(self, character):
		if self.db.is_aggro:
			self.execute_cmd("say Graaah, die %s!" % character)

class merchant(npc):
	def at_object_creation(self):
		super(merchant, self).at_object_creation()
		self.key = "Generic Merchant"
		self.db.desc = "a generic merchant, he's not supposed to be spawned like this."

	def displayItems(self, character):
		pass
