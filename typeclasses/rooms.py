"""
Room

Rooms are simple containers that has no location of their own.

"""

from evennia import DefaultRoom, TICKER_HANDLER
from world.map import Map

class Room(DefaultRoom):
    """
    Rooms are like any Object, except their location is None
    (which is default). They also use basetype_setup() to
    add locks so they cannot be puppeted or picked up.
    (to change that, use at_object_creation instead)

    See examples/object.py for a list of
    properties and methods available on all Objects.
    """
    def at_object_creation(self):
        TICKER_HANDLER.add(5, self.atmos_tick)
        self.db.temperature = 20.0
        self.db.pressure = 1.0


    def atmos_tick(self, *args, **kwargs):
	pressurePaths = {}
        pressureDiffTotal = 0.0
        for exit in self.exits:
            if self.db.pressure > exit.destination.db.pressure:
                pressureDiff = self.db.pressure - exit.destination.db.pressure
                pressureDiffTotal += pressureDiff
                pressurePaths[exit.destination] = pressureDiff

        if pressureDiffTotal != 0.0:
            pressureText = "=Atmos Detector=\n"
            for path in pressurePaths:
                movingPressure = (((pressurePaths[path] / pressureDiffTotal) * pressureDiffTotal) / len(pressurePaths)) * 0.75
                pressureText += "%s kPa moves %s.\n" % (movingPressure, path)
                self.db.pressure -= movingPressure
                path.db.pressure += movingPressure
        else:
            pressureText = "Everything seems utterly still..."

        for content in self.contents:
            if content.has_player == True:
                if content.db.detects_atmos == True:
                    content.msg(pressureText)


    def return_appearance(self, looker):
	roomText = \
	Map(looker).show_map() + '\n' + \
	super(Room, self).return_appearance(looker)

	looker.msg("%s" % roomText)
