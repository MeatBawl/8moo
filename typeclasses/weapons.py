# weapons
from typeclasses.objects import Object

 # =damage types=
 # 1 = slash
 # 2 = blunt
 # 3 = stab
 # 4 = laser
 # 5 = chemical
 # 6 = radiation

class weapon(Object):
	def at_object_creation(self):
		#super(weapon, self).at_object_creation():
		self.db.damage = 1.0
		self.db.damage_type = 1
		
#WHICH OF THESE DOES NOT BELONG
class potato(weapon):
	def at_object_creation(self):
		#super(knife, self).at_object_creation():
		self.key = "Potato"
		self.aliases.add("tuber")
		self.db.desc = "a fucking potato."
		self.db.weight = "200"
		self.db.damage = "0.1"
		damage_type = 2
 
class knife(weapon):
 	def at_object_creation(self):
		#super(knife, self).at_object_creation():
 		self.key = "Folding Knife"
 		self.aliases.add("folding")
 		self.db.desc = "a polymer-framed folder knife with an assisted opening mechanism."
 		self.db.weight = "400"
 		self.db.damage = "2.0"
		
class knuckleduster(weapon):
	def at_object_creation(self):
		#super(knuckleduster, self).at_object_creation() :
		self.key = "Knuckles"
		self.aliases.add("knucks")
		self.db.desc = "menacing brass knuckles."
		self.db.weight = "500"
		self.db.damage = "1.5"
		damage_type = 2
		
class energysword(weapon):
	def at_object_creation(self):
		#super(knuckleduster, self).at_object_creation() :
		self.key = "energysword"
		self.aliases.add("esword")
		self.db.desc = "Slices, dices, and makes julienne fries."
		self.db.weight = "300"
		self.db.damage = "6"
		damage_type = 5