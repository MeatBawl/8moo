#
# Batchcode script
#
#
# The Batch-code processor accepts full Python modules (e.g. "batch.py") that
# look identical to normal Python files with a few exceptions that allows them
# to be executed in blocks. This way of working assures a sequential execution
# of the file and allows for features like stepping from block to block
# (without executing those coming before), as well as automatic deletion
# of created objects etc. You can however also run a batch-code python file
# directly using Python.

# Code blocks are separated by python comments starting with special code words.

# #HEADER - this denotes commands global to the entire file, such as
#           import statements and global variables. They will
#           automatically be made available for each block.  Observe
#           that changes to these variables made in one block are not
#           preserved between blocks!)
# #CODE (infotext) [objname, objname, ...] - This designates a code block that
#            will be executed like a stand-alone piece of code together with
#            any #HEADER defined.
#            infotext is a describing text about what goes on in this block.
#            It will be shown by the batch-processing command.
#            <objname>s mark the (variable-)names of objects created in
#            the code, and which may be auto-deleted by the processor if
#            desired (such as when debugging the script). E.g., if the code
#            contains the command myobj = create.create_object(...), you could
#            put 'myobj' in the #CODE header regardless of what the created
#            object is actually called in-game.
# #INSERT filename - this includes another code batch file into this one. The
#            named file will be loaded and run at the position of the #INSERT.
#            Note that the inserted file will use its own #HEADERs and not
#            have access to the #HEADERs of the inserting file.

# The following variable is automatically made available for the script:
# caller - the object executing the script

#HEADER
# everything in this block will be appended to the beginning of
# all other #CODE blocks when they are executed.

from evennia import create_object, search_object
from typeclasses.objects import Object
from typeclasses.rooms import Room
from typeclasses.exits import Exit
from typeclasses.electronics import atmosDetector

#CODE
limbo = search_object("Limbo")[0]
limbo.db.desc = "welcome to the insanity"

# create Main Hub Room, assign to variable for reference
hub_main = create_object(Room, key="Main Hub")
# set ingame description
hub_main.db.desc = "Test Description"
# set sector_type (represents map symbol/other stuff)
hub_main.db.sector_type = "STREETX"
# populate room with items
create_object(atmosDetector, key="Special Detector", location=hub_main)

# create North Hub Room
hub_north = create_object(Room, key="North Hub")
# set attributes
hub_north.db.desc = "Test Description"
hub_north.db.sector_type = "STREETV"
# create Exits (this is gonna be easy to fuckup)
create_object(Exit, key="south", location=hub_north, destination=hub_main)
create_object(Exit, key="north", location=hub_main, destination=hub_north)

# repeat for South Hub
hub_south = create_object(Room, key="South Hub")
hub_south.db.desc = "Test Description"
hub_south.db.sector_type = "STREETV"
create_object(Exit, key="north", location=hub_south, destination=hub_main)
create_object(Exit, key="south", location=hub_main, destination=hub_south)

# repeat for East Hub
hub_east = create_object(Room, key="East Hub")
hub_east.db.desc = "Test Description"
hub_east.db.sector_type = "STREETH"
create_object(Exit, key="west", location=hub_east, destination=hub_main)
create_object(Exit, key="east", location=hub_main, destination=hub_east)

# repeat for West Hub
hub_west = create_object(Room, key="West Hub")
hub_west.db.desc = "Test Description"
hub_west.db.sector_type = "STREETH"
create_object(Exit, key="east", location=hub_west, destination=hub_main)
create_object(Exit, key="west", location=hub_main, destination=hub_west)

