# 8moo/world/rules.py

from random import randint

def resolve_combat(combat_handler, actiondict):
	pass

def roll(min, max):
	return randint(min, max)

def skill_speed(*args):
	char1, char2 = args
	roll1 = roll(1, 10) * char1.db.speed
	roll2 = roll(1, 10) * char2.db.speed
	if roll1 > roll2:
		return True
	else:
		return False

SKILLS = {"speed": skill_speed}

def roll_challenge(character1, character2, skillname):
	"""
	Determine the outcome of a skill challenge between
	two characters based on the skillname given. 
	"""
#	try:
	return SKILLS[skillname](character1, character2)
#	except: 
#		raise RunTimeError("Skillname %s not found." % skillname)
